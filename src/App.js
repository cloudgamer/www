import React, { Component } from 'react';

import TopStick from './components/TopStick';
import Masthead from './components/Masthead';

import style from './App.module.css';

const imageStyle = {
    marginLeft: '204px',
};

class App extends Component {
  render() {
    return (
        <TopStick>
            <Masthead>
                <center>
                    <div className={style.pageLogo}>
                        <h1>CloudGamer</h1>
                        <h2>One-click Gaming computers in the cloud.</h2>
                    </div>
                    <img src="/img/laptop.png" alt="" width="757" height="326" style={imageStyle} />

                    <div className={style.subtext}>
                        <h2>Play your favorite Steam games at full quality<br />on powerful cloud GPUs.</h2>
                    </div>
                </center>
            </Masthead>
        </TopStick>
    );
  }
}

export default App;
