import React from 'react';

import style from './Masthead.module.css';

const Masthead = (props) => {
    return (
        <div className={style.masthead}>
            {props.children}
        </div>
    );
}

export default Masthead;
