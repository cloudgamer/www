import React from 'react';

import style from './TopStick.module.css';

const TopStick = (props) => {
    return (
        <div className={style.topStick}>
            {props.children}
        </div>
    );
};

export default TopStick;
